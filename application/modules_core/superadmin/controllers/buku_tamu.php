<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class buku_tamu extends MX_Controller {

	/**
	 * @author : Gede Lumbung
	 * @web : http://gedelumbung.com
	 **/
 
   public function index($uri=0)
   {
       
		if($this->session->userdata("logged_in")!="" && $this->session->userdata("tipe_user")=="superadmin")
		{
			$this->breadcrumb->append_crumb('Dashboard', base_url().'superadmin');
			$this->breadcrumb->append_crumb("Buku Tamu", '/');
			
			$d['aktif_artikel_sekolah'] = "";
			$d['aktif_galeri_sekolah'] = "";
			$d['aktif_berita'] = "";
			$d['aktif_pengumuman'] = "";
			$d['aktif_agenda'] = "";
			$d['aktif_buku_tamu'] = "active";
			$d['aktif_list_download'] = "";
			
			$filter['nama'] = $this->session->userdata("by_nama");
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_buku_tamu($this->config->item("limit_item"),$uri,$filter);
			
			$this->load->view('bg_header',$d);
			$this->load->view('buku_tamu/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("auth/user_login");
		}
   }
 
	public function set()
	{
		if($this->session->userdata("logged_in")!="" && $this->session->userdata("tipe_user")=="superadmin")
		{
			$sess['by_nama'] = $this->input->post("by_judul");
			$this->session->set_userdata($sess);
			redirect("superadmin/buku_tamu");
		}
		else
		{
			redirect("web");
		}
   }
 
	public function hapus($id_param)
	{

		if($this->session->userdata("logged_in")!="" && $this->session->userdata("tipe_user")=="superadmin")
		{

			$where['id_super_buku_tamu'] = $id_param;
			$this->db->delete("dlmbg_super_buku_tamu",$where);
			redirect("superadmin/buku_tamu");
		}
		else
		{
			redirect("web");
		}
   }
 
	public function approve($id_param,$value)
	{

		if($this->session->userdata("logged_in")!="" && $this->session->userdata("tipe_user")=="superadmin")
		{


			//var_dump($id_param)or die();
			$id['id_super_buku_tamu'] = $id_param;
			$up['stts'] = $value;
			$this->db->update("dlmbg_super_buku_tamu",$up,$id);
			redirect("superadmin/buku_tamu");
		}
		else
		{
			redirect("web");
		}
   }
   
   public function edit($id_param)
   {
		if($this->session->userdata("logged_in")!="" && $this->session->userdata("tipe_user")=="superadmin")
		{
                  //  var_dump('ok')or die();
			$this->breadcrumb->append_crumb('Dashboard', base_url().'superadmin');
			$this->breadcrumb->append_crumb("Buku Tamu", base_url().'superadmin/buku_tamu');
			$this->breadcrumb->append_crumb("Reply Buku Tamu", '/');
			
			$d['aktif_artikel_sekolah'] = "";
			$d['aktif_galeri_sekolah'] = "";
			$d['aktif_berita'] = "";
			$d['aktif_pengumuman'] = "";
			$d['aktif_agenda'] = "active";
			$d['aktif_buku_tamu'] = "";
			$d['aktif_list_download'] = "";
			
			$where['id_super_buku_tamu'] = $id_param;
			$get = $this->db->get_where("dlmbg_super_buku_tamu",$where)->row();
			
			$d['judul'] = $get->nama;
			$d['isi'] = $get->pesan;
                        $d['kontak'] = $get->kontak;
			
			$d['id_param'] = $get->id_super_buku_tamu;
			$d['tipe'] = "edit";
			
			$this->load->view('bg_header',$d);
			$this->load->view('buku_tamu/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("auth/user_login");
		}
   }


 
   public function reply()
   {
		if($this->session->userdata("logged_in")!="" && $this->session->userdata("tipe_user")=="superadmin")
		{
                    $tipe = $this->input->post("tipe");
			$id['cek_id'] = 6;
                    //$this->input->post("id_param");
                    $query = $this->db->query('SELECT * FROM dlmbg_super_buku_tamu_reply where reply__id_bukutamu='.$id['cek_id'].' limit 1');
                    
                        foreach ($query->result() as $row)
                        {
                               $cek= $row->reply__id_bukutamu;     
                        }
                       
			if(!empty($tipe))
			{
                            // var_dump('kosong'.$cek)or die();
				//$in['reply_id'] = '1';
				$in['reply__id_bukutamu'] = $this->input->post("id_param");
				$in['reply__konten'] = $this->input->post("reply");
                                $in['reply__create_at']=time()+3600*7;
                                $in['reply__update_at']=time()+3600*7;
                                $in['reply__user__create']=$this->session->userdata("id_admin_super");
                                $in['reply__user__update']=$this->session->userdata("id_admin_super");
				
				$this->db->insert("dlmbg_super_buku_tamu_reply",$in);
                                $this->session->set_flashdata('result_insert', 'Data berhasil masuk');
			}
			else 
			{
                            // var_dump('kososng'.$cek)or die();
  
                        $id['id_super_buku_tamu'] = $id['cek_id'];
			$up['reply__konten'] = $this->input->post("reply");
			$this->db->update("dlmbg_super_buku_tamu",$up,$id);
                         $this->session->set_flashdata('result_insert', 'Data berhasil upadate');
			}
                        
			redirect("superadmin/buku_tamu");
		}
		else
		{
			redirect("auth/user_login");
		}
   }
   
}


 
/* End of file superadmin.php */
